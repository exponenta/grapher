﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GraphDrawer
{
    /// <summary>
    /// Логика взаимодействия для ColorPicker.xaml
    /// </summary>
    public partial class ColorPicker : Window
    {
        private Color startColor;
        public Color selectedColor;
        private bool fixColor = false;

        public ColorPicker(Color start)
        {
            startColor = start;
            selectedColor = startColor;
            InitializeComponent();
        }
        RenderTargetBitmap bitmap;

        private void ColorPicker_Loaded(object sender, RoutedEventArgs e)
        {
            bitmap = new RenderTargetBitmap((int)Palette.ActualWidth, (int)Palette.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bitmap.Render(Palette);

            BottomColor.Background = new SolidColorBrush(startColor);
        }

        private Color oldValue;
        public Color getColor(int x, int y)
        {
            if (bitmap != null && x < bitmap.PixelWidth && y<bitmap.PixelHeight && x > 0 && y >0)
            {
                int stride = bitmap.PixelWidth * 4;
                int size = bitmap.PixelHeight * stride;
                byte[] pixels = new byte[size];
                bitmap.CopyPixels(pixels, stride, 0);

                int index = y * stride + 4 * x;
                byte red = pixels[index + 2];
                byte green = pixels[index + 1];
                byte blue = pixels[index + 0];
                byte alpha = pixels[index + 3];

                oldValue = Color.FromArgb(alpha, red, green, blue);
            }

            return oldValue;
        }

        private void Palette_MouseMove(object sender, MouseEventArgs e)
        {
            if (!fixColor)
            {
                Point pos = e.GetPosition(Palette);
                TopColor.Background = new SolidColorBrush(getColor((int)pos.X, (int)pos.Y));
            }
        }

        private void Palette_MouseDown(object sender, MouseButtonEventArgs e)
        {
            fixColor = !fixColor; 
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            if (fixColor)
                selectedColor = oldValue;
            MainWindow mw = this.Owner as MainWindow;
            mw.OnCloseColorPicker(selectedColor, true);
            Close();
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {

            if (fixColor)
                selectedColor = oldValue;
            MainWindow mw = this.Owner as MainWindow;
            mw.OnCloseColorPicker(selectedColor, false);
            Close();
        }
    }
}
