﻿using System;
using System.Windows;
using ExpressionEvaluator;
using ExpressionEvaluator.Parser;

namespace GraphDrawer
{
    /// <summary>
    /// Логика взаимодействия для MathGraph.xaml
    /// </summary>
    public partial class MathGraph : Window
    {

        private MathGraphContainer mgc;
        private string old_expression = "";
        public MathGraph(MathGraphContainer m)
        {
            mgc = m;
            InitializeComponent();

            if (mgc.expression != null)
            {
                old_expression = mgc.expression.StringToParse;
                textExpression.Text = mgc.expression.StringToParse;
            }
        }

        private void Compile_Click(object sender, RoutedEventArgs e)
        {
            // MathGraphContainer 

            try
            {
                mgc.Compile(textExpression.Text);
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Ошибка парсинга!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Compile_Click(sender, e);
            if (mgc.wasCompiled)
            {
                ((MainWindow)(Owner)).MathWindowClosed(mgc);
                Close();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            mgc.expression.StringToParse = old_expression;
            Close();
        }
    }
}
