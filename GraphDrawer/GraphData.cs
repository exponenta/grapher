﻿using System;
using System.Data;
using System.IO;
using System.Windows;

namespace GraphDrawer
{
    public class GraphData
    {

        public DataTable data; 
        public TxtParseConfig txtParseConfig;


        public class TxtParseConfig
        {
            public string valueSeparator = " ";
            public string lineSeparator = "\r\n";
            public bool hasHeaderNames = false;
            public double badValueFiller = 0.0;
        }

        public GraphData()
        {
            txtParseConfig = new TxtParseConfig();
            this.data = new DataTable("File");
        }

        public GraphData(string filename) : this()
        {
            if (filename.EndsWith("txt") || filename.EndsWith("csv"))
            {
                string data = "";
                try
                {
                    data = File.ReadAllText(filename);

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Parsing error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                string[] lines = data.Split(new string[] { txtParseConfig.lineSeparator }, StringSplitOptions.None);

                int count = 0;

                // Индексы
                //headers.Add("Index");
                DataColumn c = new DataColumn("Index", typeof(double));
                c.ReadOnly = true;
                this.data.Columns.Add(c);

                for (int i = 0; i < lines.Length; i++)
                {
                    string[] splided = lines[i].Split(new string[] { txtParseConfig.valueSeparator }, StringSplitOptions.None);
                    if (i == 0)
                    {

                        count = splided.Length;
                        if (txtParseConfig.hasHeaderNames)
                        {
                            //headers.AddRange(splided);
                            for (int k = 0; k < count; k++)
                                this.data.Columns.Add(splided[k], typeof(double));
                                continue;
                        }
                        else
                        {
                            for (int k = 0; k < count; k++)
                                this.data.Columns.Add("CT" + k, typeof(double));
                        }
                    }

                    // длина + индексы
                    DataRow gdr = this.data.NewRow();

                    gdr["Index"] = i;
                    for (int j = 0; j < count; j++)
                    {

                        gdr[j + 1] = txtParseConfig.badValueFiller;
                        double tmp;
                        if (splided.Length > j)
                        {
                            if (splided.Length <= count && double.TryParse(splided[j], out tmp))
                            {
                                gdr[j + 1] = tmp;
                            }
                        }
                    }

                    this.data.Rows.Add(gdr);
                }

            }
            else
            {
                //TODO ExelParser
                //Потом Как-нибудь)))
            }
        }

    }
}
