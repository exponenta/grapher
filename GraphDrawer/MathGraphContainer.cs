﻿using ExpressionEvaluator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace GraphDrawer
{
   public class MathGraphContainer : GraphContainer
    {
        private Func<Caller,double> transformer;
        public List<double> yAxis;
        public List<double> xAxis;
        private Caller caller;
        public CompiledExpression<double> expression;
        public string stopedMessage;
        public bool wasCompiled { get; private set; } = false;

        public class Caller
        {
            public List<GraphContainer> graphs;
            public int Index = 0;
            public List<double> xAxis;
            public bool interated = true;

            public Caller(List<GraphContainer> list) { graphs = list; }

            public double graph(string name)
            {
                var g = graphs.Where(p => p.Name == name).ToArray();
                if(g.Length > 0)
                {
                    if (xAxis == null)
                        xAxis = g[0].GetXAxis();
                    if (Index < xAxis.Count)
                    {
                        return g[0].GetValue(Index);

                    } else
                    {
                        interated = false;
                        return 0;
                    }
                }

                throw new MissingMemberException($"Graph with name {name} not found!");
            }

            public void Reset()
            {
                Index = 0;
                interated = true;
                xAxis = null;
            }
        }

        public MathGraphContainer(string name, Color color) : base(name, color)
        {
            expression = new CompiledExpression<double>();
            TypeRegistry tr = new TypeRegistry();
            tr.RegisterSymbol("m", typeof(Math));
        }

        public void SetGraphsList(List<GraphContainer> list)
        {
            caller = new Caller(list);
        }

        public void Compile(string txt)
        {
            expression.StringToParse = txt;
            if (txt == "")
                throw new Exception("Выражение не может быть пустым!");
            Compile();
        }


        public void Compile()
        {
            transformer = expression.ScopeCompile<Caller>();

            caller.Reset();

            transformer(caller);
            xAxis = caller.xAxis;
            if (xAxis != null)
            {
                yAxis = new List<double>(xAxis.Count);

                for (int i = 0; i < xAxis.Count; i++)
                {
                    caller.Index = i;
                    if (!caller.interated)
                        break;
                    yAxis.Add(transformer(caller));
                }

                dirty = true;
                wasCompiled = true;
            } else
            {
                throw new Exception("Необходо иметь хоть одну ссылку на график в выражении!");
            }
        }

        public override void ForceUpdateGraph()
        {
            if (!wasCompiled)
                return;

            if(dirty)
            {
                Reconstruct();
            } else {
                float grid_s = Params.scaled_grid_size;
                

                foreach (PathFigure pf in _graph_pfc)
                {
                    pf.StartPoint = new Point(xAxis[0] * grid_s, yAxis[0] * grid_s) + Params.drag_offset;
                    
                    pf.StartPoint = new Point(xAxis[0] * grid_s, yAxis[0] * grid_s) + Params.drag_offset;

                    for (int i = 1; i < yAxis.Count; i++)
                    {
                        PathSegment seg = pf.Segments[i - 1];
                        
                        if (seg is LineSegment)
                        {
                            ((LineSegment)seg).Point = new Point(xAxis[i] * grid_s, yAxis[i] * grid_s) + Params.drag_offset;
                        }
                    }

                }

            }
        }

        private void Reconstruct()
        {
            float grid_s = Params.scaled_grid_size;

            _graph_pfc.Clear();

            PathFigure pf = new PathFigure();
            pf.IsClosed = false;
            pf.IsFilled = false;

            pf.StartPoint = new Point(xAxis[0] * grid_s, yAxis[0] * grid_s) + Params.drag_offset;

            //_data.rows.Sort( (a, b) => a.data[_xitem].CompareTo(b.data[_xitem]) );

            for (int i = 1; i < yAxis.Count; i++)
            {
                Point p = new Point(xAxis[i] * grid_s, yAxis[i] * grid_s) + Params.drag_offset;
                pf.Segments.Add(new LineSegment(p, true));
            }

            _graph_pfc.Add(pf);

            (_graph.Data as PathGeometry).Figures = _graph_pfc;
        }

        public override double GetValue(int index)
        {
            return yAxis[index];
        }
        public override List<double> GetXAxis()
        {
            return xAxis;
        }
    }
}
