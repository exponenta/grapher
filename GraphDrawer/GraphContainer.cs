﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GraphDrawer
{
    public class GraphContainer : INotifyPropertyChanged
    {
        protected SolidColorBrush _color;
        protected GraphData _data;
        protected Path _graph;
        protected PathFigureCollection _graph_pfc;
        private int _xitem;
        private int _yitem;
        protected bool dirty = true;
        public DrawParam Params;
        public string Name { get; set; }

        public SolidColorBrush Color {
            get {
                return _color;
            }
            set {
                _color = value;
                OnPropertyChanged("Color");
                if (_graph != null)
                {
                    _graph.Stroke = _color;
                    _graph.Fill = _color;
                }
            }
        }
        public Control control;
        
        public GraphData Data { set { _data = value; if (value != null) OnPropertyChanged("DataName"); } get { return _data; } }
        public Path Graph { get { return _graph; } }

        public List<string> DataNames
        {
            get
            {
                if (_data != null)
                    return _data.data.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList<string>();
                return null;
            }
        }

        public int XItem {
            get {
                return _xitem;
            }
            set {
                dirty = _xitem != value || dirty;
                _xitem = value;

            }
        }

        public int YItem
        {
            get
            {
                return _yitem;
            }
            set
            {
                dirty = _yitem != value || dirty;
                _yitem = value;

            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public GraphContainer(string name, Color color)
        {
            XItem = 0;
            YItem = 0;
            Name = name;
            Color = new SolidColorBrush(color);

            _graph = new Path();
            _graph_pfc = new PathFigureCollection();
            _graph.Data = new PathGeometry(_graph_pfc);
            _graph.Stroke = _color;
            _graph.StrokeEndLineCap = PenLineCap.Round;
            _graph.StrokeLineJoin = PenLineJoin.Bevel;
            _graph.Fill = _color;
            _graph.StrokeThickness = 2;

        }
        

        public virtual void ForceUpdateGraph()
        {

            if (_xitem < 0 || _yitem < 0 || _xitem == _yitem)
                return;

            if (dirty)
            {
                Reconstruct();
                dirty = false;

            } else
            {

                float grid_s = Params.scaled_grid_size;
                DataColumn dcx = _data.data.Columns[_xitem];
                DataColumn dcy = _data.data.Columns[_yitem];

                foreach (PathFigure pf in _graph_pfc)
                {
                    double x = _data.data.Rows[0].Field<double>(dcx);
                    double y = _data.data.Rows[0].Field<double>(dcy);

                    pf.StartPoint = new Point(x * grid_s, y * grid_s) + Params.drag_offset;

                    for (int i = 1; i < _data.data.Rows.Count; i++)
                    {
                        PathSegment seg = pf.Segments[i - 1];

                        x = _data.data.Rows[i].Field<double>(dcx);
                        y = _data.data.Rows[i].Field<double>(dcy);

                        if (seg is LineSegment) {
                            ((LineSegment)seg).Point = new Point(x * grid_s, y * grid_s) + Params.drag_offset;
                        }
                    }

                }
            }
        }
        
        private void Reconstruct()
        {

            float grid_s = Params.scaled_grid_size;

            _graph_pfc.Clear();

            PathFigure pf = new PathFigure();
            pf.IsClosed = false;
            pf.IsFilled = false;


            DataColumn dcx = _data.data.Columns[_xitem];
            DataColumn dcy = _data.data.Columns[_yitem];

            double x = _data.data.Rows[0].Field<double>(dcx);
            double y = _data.data.Rows[0].Field<double>(dcy);
            pf.StartPoint = new Point(x * grid_s, y * grid_s) + Params.drag_offset;

            //_data.rows.Sort( (a, b) => a.data[_xitem].CompareTo(b.data[_xitem]) );

            for (int i = 1; i < _data.data.Rows.Count; i++)
            {
                x = _data.data.Rows[i].Field<double>(dcx);
                y = _data.data.Rows[i].Field<double>(dcy);

                Point p= new Point(x * grid_s, y * grid_s) + Params.drag_offset;
                pf.Segments.Add(new LineSegment(p, true));
            }

            _graph_pfc.Add(pf);
           
            (_graph.Data as PathGeometry).Figures = _graph_pfc;
        }

        public List<Point> GetIntersection(Line line)
        {
            List<Point> result = new List<Point>(3);

            foreach (PathFigure pf in _graph_pfc)
            {
                Point from = pf.StartPoint - Params.scaled_drag_offset;
                Point to;
                for (int i = 0; i < pf.Segments.Count; i++)
                {
                    LineSegment seg = (LineSegment)pf.Segments[i];
                    to = seg.Point;

                    //вертикаль 
                    if(line.X1 == line.X2)
                    {
                        if (from.X >= line.X1 && to.X < line.X1)
                        {
                            double ny = from.Y + (line.X1 - from.X) * (to.Y - from.Y) / (to.X - from.X);
                            Point ins = new Point(line.X1, ny);
                            result.Add(ins);
                        }
                    } else
                    {
                        if (from.Y >= line.Y1 && to.Y < line.Y1)
                        {
                            double nx = from.X + (line.Y1 - from.Y) * (to.X - from.X) / (to.Y - from.Y);
                            Point ins = new Point(line.X1, line.Y1);
                            result.Add(ins);
                        }
                    }
                    from = to;
                }

            }
            return result;
        }

        public virtual double GetValue(int index)
        {
            return _data.data.Rows[index].Field<double>(_yitem);
        }

        public virtual List<double> GetXAxis()
        {
            if (_data == null)
                return null;
            List<double> list = new List<double>(_data.data.Rows.Count);

            for(int i=0; i < _data.data.Rows.Count; i++)
            {
                list.Add(_data.data.Rows[i].Field<double>(_xitem));
            }
            return list;
        }

    }
}
