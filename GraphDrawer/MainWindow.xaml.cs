﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GraphDrawer
{
    public class Cursor
    {
        public Line line;
        public TextBlock text;
        public bool vertial = false;
        private DrawParam param;

        public Cursor(DrawParam param, bool ver)
        {
            vertial = ver;
            this.param = param;
            line = new Line()
            {
                X1 = 0,
                X2 = 0,
                Y1 = 0,
                Y2 = param.canvas.ActualHeight,
                StrokeThickness = 1,
                Stroke = new SolidColorBrush(Colors.White),
                StrokeDashArray = { 2f, 1f }
            };
            
            text = new TextBlock();
            text.Text = "0";
            text.Foreground = new SolidColorBrush(Colors.White);
            Canvas.SetLeft(text, 100);
            Canvas.SetTop(text, 100);
            text.LayoutTransform = (Transform)param.canvas.LayoutTransform.Inverse;
        }

        public void SetPosition(Point pos)
        {
            if (vertial)
            {
                line.X1 = line.X2 = pos.X;
                line.Y1 = param.drag_offset.Y;
                line.Y2 = pos.Y;

                double y = Math.Max(param.drag_offset.Y + param.grid_size * 0.1, 0);
                y = Math.Min(y, param.canvas.ActualHeight - text.ActualHeight);

                Canvas.SetTop(text, y);
                Canvas.SetLeft(text, pos.X);
            }
            else
            {
                line.X1 = pos.X;
                line.X2 = param.drag_offset.X;
                line.Y1 = line.Y2 = pos.Y;
                
                double x = Math.Max(param.drag_offset.X - text.ActualWidth - param.grid_size * 0.1, 0);
                x = Math.Min(x, param.canvas.ActualWidth - text.ActualWidth);

                Canvas.SetLeft(text, x);
                Canvas.SetTop(text, pos.Y);
            }
        }

        public void SetText(string txt)
        {
            text.Text = txt;
        }

        public void Reparent() {
            if (!param.canvas.Children.Contains(line))
                param.canvas.Children.Add(line);
            if (!param.canvas.Children.Contains(text))
                param.canvas.Children.Add(text);

        }
    }

    /// <summary>
    /// Вспомогательный класс с параметрами рисования, что бы не пересчитывать 1000 раз;
    /// </summary>
    /// 
    public class DrawParam : INotifyPropertyChanged
    {

        private float _scaleFactor = 1;

        public Canvas canvas;
        public Vector drag_offset = new Vector(0, 0);
        public float grid_size = 50;
        public bool showLabels { get; set; } = true;

        public Vector scaled_drag_offset
        {
            get
            {
                return new Vector(drag_offset.X * scaleFactor, drag_offset.Y * scaleFactor);
            }
        }
        public float scaled_grid_size
        {
            get
            {
                return grid_size * scaleFactor;
            }
        }

        public float minScaleFactor { get; set; } = 0.1f;
        public float maxScaleFactor { get; set; } = 5f;

        public float scaleFactor
        {
            get { return _scaleFactor; }
            set
            {
                _scaleFactor = value;
                OnPropertyChanged("scaleFactor");
            }
        }

        //Минимальный размер сетки, после чего она будет прорежаться
        public float minGridSize { get; set; } = 50f;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public partial class MainWindow : Window
    {
        public GraphData graphData;
        public List<GraphContainer> graphContainerList = new List<GraphContainer>();
        public List<MathGraphContainer> mathGraphContainerList = new List<MathGraphContainer>();

        public List<Control> graphControlList;

        public List<Line> gridLines = new List<Line>();
        public DrawParam Params = new DrawParam();
        public List<Cursor> cursors = new List<Cursor>(2);

        public MainWindow()
        {
            InitializeComponent();
            Params.canvas = Canvas;
            
            for(int i=0; i < cursors.Capacity; i++)
            {
                cursors.Add(new Cursor(Params, false));
            }
        }

        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Точно хотите выйти?", "Закрытие", MessageBoxButton.YesNo);
            if (result != MessageBoxResult.Yes)
                e.Cancel = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ScaleFactor.DataContext = Params;
        }

        private void NenuItem_Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files(*.txt) |*.txt";//|Excel files (*.xlsx) |*.xlsx";
            dialog.Title = "Выберите файлы для открытия";
            if (dialog.ShowDialog() == true)
            {
                graphData = new GraphData(dialog.FileName);
                UpdateDataGridViewer();
            }
        }

        private void UpdateDataGridViewer()
        {
            if (graphData != null)
            {
                dataGrid.DataContext = graphData.data.DefaultView;

                foreach (GraphContainer gc in graphContainerList)
                {
                    gc.Data = graphData;
                }
            }
        }

        private void AddNewGraphButton_Click(object sender, RoutedEventArgs e)
        {
            var tmp = new GraphContainer("Graph_" + graphContainerList.Count, Colors.Green);

            tmp.Data = graphData;
            tmp.Params = Params;
            GroupItem control = new GroupItem();
            control.Template = (ControlTemplate)FindResource("GraphEditorItem");
            control.DataContext = tmp;

            graphControllerPanel.Children.Add(control);
            tmp.control = control;
            graphContainerList.Add(tmp);


        }

        private ColorPicker cp;
        private Control activator;

        private void Color_Click(object sender, RoutedEventArgs e)
        {

            activator = (Control)sender;

            GraphContainer gc = activator.DataContext as GraphContainer;
            if (gc != null)
            {
                if (cp == null)
                {
                    cp = new ColorPicker(gc.Color.Color);
                    cp.Owner = this;
                    cp.Show();
                }
                else
                {
                    cp.Focus();
                }
            }
        }

        public void OnCloseColorPicker(Color color, bool okPressed)
        {
            if (okPressed)
            {
                GraphContainer gc = activator.DataContext as GraphContainer;
                if (gc != null)
                {
                    gc.Color = new SolidColorBrush(color);
                }
            }
            cp = null;
            activator = null;
        }

        private void DeletGraph_Click(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;
            GraphContainer gc = control.DataContext as GraphContainer;
            if (MessageBox.Show("Вы точно хотите удалить график?", "Удаление графика", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                graphControllerPanel.Children.Remove(gc.control);
                graphContainerList.Remove(gc);
                Canvas_Rerender();
            }
        }

        Brush defGridBrush;
        Brush defAxisBrush;

        private void UpdateGridCanvas()
        {
            float grid_size = Params.scaled_grid_size;

            int x_count = (int)Math.Ceiling(Canvas.ActualWidth / grid_size);
            int y_count = (int)Math.Ceiling(Canvas.ActualHeight / grid_size);

            float delta_x = (float)(Params.drag_offset.X % grid_size);
            float delta_y = (float)(Params.drag_offset.Y % grid_size);

            if (defGridBrush == null)
                defGridBrush = FindResource("DefaultGridBrush") as SolidColorBrush;

            if (defAxisBrush == null)
                defAxisBrush = FindResource("DefaultAxisBrush") as SolidColorBrush;

            int step = (int)Math.Ceiling(Params.minGridSize / grid_size);

            for (int i = 0; i < y_count + 1; i++)
            {
                int realIndex = i - (int)Math.Floor(Params.drag_offset.Y / grid_size);
                //Ибо непонятино почему при смещении вниз, градация смещается вверх
                if (Params.drag_offset.Y < 0)
                    realIndex -= 1;

                if (realIndex % step != 0 && realIndex != 0)
                    continue;

                Line line = new Line()
                {
                    X1 = 0,
                    X2 = Canvas.ActualWidth,
                    Y1 = i * grid_size + delta_y,
                    Y2 = i * grid_size + delta_y,
                    Stroke = defGridBrush,
                    Fill = defGridBrush
                };

                // Эта линия у нас оказалась осью ;-)
                if (Math.Abs(line.Y1 - Params.drag_offset.Y) < 0.01f)
                {
                    line.Stroke = defAxisBrush;
                    line.Fill = defAxisBrush;
                    line.StrokeThickness = 2;
                }

                Canvas.Children.Add(line);

                //Marker - штрихи
                Line marker = new Line()
                {
                    Y1 = i * grid_size + delta_y,
                    Y2 = i * grid_size + delta_y,
                    X1 = Params.drag_offset.X + Params.grid_size * .1,
                    X2 = Params.drag_offset.X - Params.grid_size * .1,
                    Stroke = defAxisBrush,
                    Fill = defAxisBrush
                };

                Canvas.Children.Add(marker);

                if (Params.showLabels)
                {
                    TextBlock textBlock = new TextBlock();
                    textBlock.Text = realIndex.ToString();
                    textBlock.Foreground = defAxisBrush;
                    Canvas.SetTop(textBlock, i * grid_size + delta_y);
                    Canvas.SetLeft(textBlock, Params.drag_offset.X + Params.grid_size * .2);
                    textBlock.LayoutTransform = (Transform)Canvas.LayoutTransform.Inverse;
                    Canvas.Children.Add(textBlock);
                }
            }

            for (int i = 0; i < x_count + 1; i++)
            {

                int realIndex = i - (int)Math.Floor(Params.drag_offset.X / grid_size);
                //Ибо непонятино почему при смещении вниз, градация смещается вверх
                if (Params.drag_offset.X < 0)
                    realIndex -= 1;

                if (realIndex % step != 0 && realIndex != 0)
                    continue;

                Line line = new Line()
                {
                    X1 = i * grid_size + delta_x,
                    X2 = i * grid_size + delta_x,
                    Y1 = 0,
                    Y2 = Canvas.ActualHeight,
                    Stroke = defGridBrush,
                    Fill = defGridBrush
                };

                Canvas.Children.Add(line);

                // Как и эта линия у нас оказалась осью ;-)
                if (Math.Abs(line.X1 - Params.drag_offset.X) < 0.01f)
                {
                    line.Stroke = defAxisBrush;
                    line.Fill = defAxisBrush;
                    line.StrokeThickness = 2;
                }

                //Marker
                Line marker = new Line()
                {
                    X1 = i * grid_size + delta_x,
                    X2 = i * grid_size + delta_x,
                    Y1 = Params.drag_offset.Y + Params.grid_size * .1,
                    Y2 = Params.drag_offset.Y - Params.grid_size * .1,
                    Stroke = defAxisBrush,
                    Fill = defAxisBrush
                };

                Canvas.Children.Add(marker);

                if (Params.showLabels && realIndex != 0)
                {
                    TextBlock textBlock = new TextBlock();
                    textBlock.Text = realIndex.ToString();
                    textBlock.Foreground = defAxisBrush;
                    Canvas.SetLeft(textBlock, i * grid_size + delta_x);
                    Canvas.SetTop(textBlock, Params.drag_offset.Y - Params.grid_size * .4);
                    textBlock.LayoutTransform = (Transform)Canvas.LayoutTransform.Inverse;
                    Canvas.Children.Add(textBlock);
                }
            }

        }

        private void Canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            int delta = e.Delta;
            float sd = delta > 0 ? 1.1f : 0.9f;

            Params.scaleFactor *= sd;

            Params.scaleFactor = Math.Max(Params.scaleFactor, Params.minScaleFactor);
            Params.scaleFactor = Math.Min(Params.scaleFactor, Params.maxScaleFactor);

            if (Params.scaleFactor != Params.minScaleFactor && Params.scaleFactor != Params.maxScaleFactor)
            {
                Vector pos = (Vector)e.GetPosition(Canvas);
                Vector dpos = (pos - Params.drag_offset) * (sd - 1f);
                Params.drag_offset -= dpos;

                Canvas_Rerender();
                UpdateCursors((Point)pos);
            }
        }

        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Params.drag_offset.X == 0 && Params.drag_offset.Y == 0)
            {
                Params.drag_offset.X = Canvas.ActualWidth / 2;
                Params.drag_offset.Y = Canvas.ActualHeight / 2;
            }
            //UpdateCursors(new Point(0, 0));
            Canvas_Rerender();
        }

        private Point old_pos = new Point(0, 0);

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(Canvas);


            if (e.MiddleButton == MouseButtonState.Pressed)
            {
                if (pos != old_pos)
                {
                    Params.drag_offset += (pos - old_pos);
                    Canvas_Rerender();

                }
            }

            UpdateCursors(pos);
            old_pos = pos;

        }

        private void ComBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Control s = (Control)sender;
            GraphContainer gc = s.DataContext as GraphContainer;
            gc.ForceUpdateGraph();
            Canvas_Rerender();
        }


        private void UpdateCursors(Point pos)
        {
            Point gpoint = GlobalToGraph(pos);

            float step = (float)Math.Ceiling(Params.minGridSize / Params.grid_size);

            cursors[0].SetPosition(pos);
            cursors[0].vertial = true;

            float x = (float)gpoint.X;
            float midX = (float)Math.Floor(x / step) * step;

            if (Math.Abs(x - midX) < (1 / Params.scaled_grid_size))
            {
                x = midX;
            }

            cursors[0].SetText($"{x:.00}");

            cursors[1].SetPosition(pos);


            float y = (float)gpoint.Y;
            float midY = (float)Math.Floor(y / step) * step;

            if (Math.Abs(y - midY) < (1 / Params.scaled_grid_size))
            {
                y = midY;
            }

            cursors[1].SetText($"{y:.00}");

        }

        private Point GraphToGlobal(Point gp)
        {
            gp.X = Params.drag_offset.X + Params.grid_size * gp.X / Params.scaleFactor;
            gp.Y = Params.drag_offset.Y + Params.grid_size * gp.Y / Params.scaleFactor;
            return gp;
        }

        private Point GlobalToGraph(Point gp)
        {
            Point ret = new Point();
            ret.X = (-Params.drag_offset.X + gp.X) / Params.scaled_grid_size;
            ret.Y = (-Params.drag_offset.Y + gp.Y) / Params.scaled_grid_size;
            return ret;
        }


        private void Canvas_Rerender()
        {
            Canvas.Children.Clear();
            UpdateGridCanvas();

            foreach (GraphContainer gc in graphContainerList)
            {
                gc.ForceUpdateGraph();
                Canvas.Children.Add(gc.Graph);
            }
            
            foreach (var c in cursors)
            {
                c.Reparent();
            }
        }

        private void AddNewMathGraphButton_Click(object sender, RoutedEventArgs e)
        {
            MathGraphContainer tmp = new MathGraphContainer("Math_" + graphContainerList.Count, Colors.Red);
            tmp.SetGraphsList(graphContainerList);
            MathGraph mgw = new MathGraph(tmp);
            mgw.Owner = this;
            mgw.Show();

        }

        public void MathWindowClosed(MathGraphContainer mgc)
        {
            if(mgc != null)
            {
                if (mgc.control == null)
                {
                    mgc.Params = Params;
                    GroupItem control = new GroupItem();
                    control.Template = (ControlTemplate)FindResource("GraphEditorMathItem");
                    control.DataContext = mgc;

                    graphControllerPanel.Children.Add(control);
                    mgc.control = control;
                    graphContainerList.Add(mgc);
                }
                Canvas_Rerender();
            }
        }
        private void dataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {

            //пересобрать графики 
            foreach (GraphContainer gc in graphContainerList)
            {

                Canvas.Children.Remove(gc.Graph);
                gc.ForceUpdateGraph();
                Canvas.Children.Add(gc.Graph);

            }
        }

        private void OpenSettings_Click(object sender, RoutedEventArgs e)
        {
            var gc = ((Control)sender).DataContext;

            if(gc is MathGraphContainer)
            {
                MathGraph mgw = new MathGraph(gc as MathGraphContainer);
                mgw.Owner = this;
                mgw.Show();
            }
        }
    }
}
