# README! #
## GraphDrawer ##
Простенький рисовальщик графиков, состоящий из 2х версий

* WPFBased - отрисовка графиков на чистом WPF
* WPF + OpenGlBased - отрисовка OpenGL в контейнер WPF

### Авторство ###
  Тимошенко (eXponenta) Константин. НГТУ ФПМИ 2016 год 
### Скрины: ###
#WPFbased#
![WPF](https://bitbucket.org/repo/yLd5Ro/images/3085227239-6Vf0YOPwbV0%5B1%5D.jpg)

#WPF + OpenGL Based#
![JZNde3RvcYM[1].jpg](https://bitbucket.org/repo/yLd5Ro/images/2286203442-JZNde3RvcYM%5B1%5D.jpg)

### Доступность ###

Исходные коды доступны под лицензией GPL3